PLUGIN.Title = "Silent Mute"
PLUGIN.Description = "Silent-Mute a player based on his chat message"
PLUGIN.Author = "Azunai"
PLUGIN.Version = "0.1"


function PLUGIN:Init()
  print("Silen-Mute loading.......")
  CensorListFile = util.GetDatafile("silent-mute_censorlist")
  CensorListText = CensorListFile:GetText()
  if not(CensorListText == "") then
    CensorList = json.decode(CensorListText)
  else
    CensorList = {}
  end

  self:AddChatCommand("muteadd",self.Mute)
  self:AddChatCommand("mutereload",self.Reload)
  
end

-- Saves the censor list data file to the server.
function PLUGIN:SaveCensorList()
  CensorListFile:SetText(json.encode(CensorList))
  CensorListFile:Save()
end

function PLUGIN:Reload( netuser, cmd, args )
  if netuser:CanAdmin() then
    plugins.Reload("silent-mute")
  else
    rust.Notice(netuser, "You are not an admin!")
  end
end

function PLUGIN:Mute( netuser, cmd, args )
  if netuser:CanAdmin() then
    CensorList[args[1]] = true
    rust.Notice(netuser,"Added "..tostring(args[1]).." to CensorList")
    self.SaveCensorList()
  else
    rust.Notice(netuser, "You are not an admin!")
  end
end

function IsIP(ip)
  -- must pass in a string value
  if ip == nil or type(ip) ~= "string" then
    return 0
  end

  -- check for format 1.11.111.111 for ipv4
  local chunks = {ip:match("(%d+)%.(%d+)%.(%d+)%.(%d+)")}
  if (#chunks == 4) then
    for _,v in pairs(chunks) do
      if (tonumber(v) < 0 or tonumber(v) > 255) then
        return false
      end
    end
    return true
  else
    return false
  end
end

function IsCensored(msg)
  for k,v in pairs(CensorList) do
    if (msg:find(k) ~= nil)then
      return true
    end
  end
  return false
end



function PLUGIN:OnUserChat( netuser, name, msg )
  if (msg:sub( 1, 1 ) ~= "/") then
    if (IsIP(msg) or IsCensored(msg)) then
      rust.SendChatToUser( netuser, name, msg)
      return true
    end
    rust.BroadcastChat(  name, msg )
    return true
  end
end

